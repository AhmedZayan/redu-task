<?php

namespace App\Console\Commands;

use App\Services\PinsGeneratorService;
use Illuminate\Console\Command;

class generatePins extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate-pins';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates 4 digit numeric pins according to specific rules';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $service=new PinsGeneratorService();
        $service->generate();
    }
}
