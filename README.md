##Redu Task written in laravel 8 version

- Clone the repo
- Run composer install
- Make a copy of .env from .env.example
- Put your SMTP email credentials in .env to receive emails(I'm using mailtrap for just testing, not rial emails)
- Run database migration (here I'm using mysql, but you can change it if you want)
- Run php artisan generate-pins (to generate and store the pins list according to task rules)
- Register user then you will receive an email with the pin(a pin selected according to task rules)
 
 
