<?php

namespace App\Http\Controllers;

use App\Services\PinsGeneratorService;

class PinController extends Controller
{
    public function generateUniquePins(PinsGeneratorService $service)
    {
        $pins=$service->generate();

        return view('pins',compact('pins'));
    }
}
