<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Pins') }}
        </h2>
    </x-slot>

    @foreach($pins as $pin)
        {{ $pin }}<br>
    @endforeach
</x-app-layout>
