<?php


namespace App\Services;


use App\Models\Pin;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PinsGeneratorService
{
    //Generates the random un sorted pins list and stores it to pins table
    public function generate()
    {
        $excludedPins=[];
        $pins=range(1000,9999);
        foreach ($pins as $pin)
        {
            if ($this->checkPinDigitsIsSame($pin))
            {
                $excludedPins[]=$pin;
            }
        }

        $pins=array_diff($pins,$excludedPins);

        shuffle($pins);

        $this->storePins($pins);

        return $pins;
    }

    //Get pin from unused pins or if all pins is used select random one
    public function getPin()
    {
        $unused_pins_count=Pin::where('is_used',0)->count();
        if ($unused_pins_count>0)
        {
            $pin=Pin::where('is_used',0)->orderBy(DB::raw('RAND()'))->first();
            $pin->update(['is_used'=>1]);
        }
        else
        {
            $pin=Pin::orderBy(DB::raw('RAND()'))->first();
        }

        return $pin->pin;
    }

    //Checks if the number digits is the same
    private function checkPinDigitsIsSame($pin)
    {
        $digit = $pin % 10;
        while ($pin !== 0)
        {
            $current_digit = $pin % 10;
            $pin = (int)($pin/ 10);
            if ($current_digit !== $digit)
            {
                return false;
            }
        }

        return true;
    }

    //Store pins to database
    private function storePins($pins)
    {
        foreach ($pins as $pin)
        {
            Pin::create([
                'pin'=>$pin
            ]);
        }
    }
}
